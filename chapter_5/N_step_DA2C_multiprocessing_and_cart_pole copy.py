import torch
from torch import nn
from torch import optim
import numpy as np
from torch.nn import functional as F
import gym
import torch.multiprocessing as mp
from multiprocessing import Manager
import matplotlib.pyplot as plt

# N step DA2C (update-ovacemo parametre mreze nakon izvrsenih N koraka)

# N-step treniranje bi trebalo da bude bolje od online treniranja(1-step), 
# jer ce za state proizvesti q_value koja ima manji bias(u manjoj meri odstupa od prave vrednosti) nego sto bi imala sa 1-step treniranjem(online)

# bootstraping - making prediction from a prediction(trebalo bi da smanjuje varijansu kod rezultata)

# chapter: 5, page: 126
class ActorCritic(nn.Module):
    def __init__(self):
        super(ActorCritic, self).__init__()

        self.l1 = nn.Linear(4, 25)
        self.l2 = nn.Linear(25, 50)

        self.actor_lin1 = nn.Linear(50, 2)

        self.l3 = nn.Linear(50, 25)

        self.critic_lin1 = nn.Linear(25, 1)

    def forward(self, x):
        x = F.normalize(x, dim=0)
        y = F.relu(self.l1(x))
        y = F.relu(self.l2(y))

        actor = F.log_softmax(self.actor_lin1(y), dim=0)

        # chapter: 5, page: 127
        # pozivamo detach fn nad y node-om i to otkacinje taj node od grafa,
        # jer ne zelimo da kriticarevu gresku propagiramo unazad i da ona modifikuje tezine na
        # lejerima l1 i l2
        
        # zelimo da samo greska od aktora utice na tezine lejera l1 i l2
        # dakle buduci da ovde imamo 'neprijateljski' odnos izmedju aktora i kriticara, moramo nekome
        # da damo prednost i da neko utice na tezine od samog pocetka
        c = F.relu(self.l3(y.detach()))

        # https://pytorch.org/docs/stable/generated/torch.nn.Tanh.html
        # Tanh aktivaciona f-ja ce svesti izlaz na range (-1, 1) sto nam odgovara
        # jer su nagrade u CartPole-u -1 ili +1
        critic = torch.tanh(self.critic_lin1(c))

        return actor, critic

# chapter: 5, page: 133
def run_episode(env, model, N_steps=10):
    state_np = np.array(env.env.state)
    state = torch.from_numpy(state_np).float()
    values, taken_action_log_probs, rewards = [], [], []
    done = False
    j = 0
    # definisemo da je krajnja nagrada 0 u slucaju da zaista i dodjemo do kraja epizode pre nego izvrsimo vise koraka od N_steps
    G = torch.Tensor([0])
    episode_ended = False
    while (j < N_steps and done == False):
        j += 1
        log_probs, q_value = model(state)
        values.append(q_value)
        logits = log_probs.view(-1) # view(-1) -> da bismo bili sigurni da je 1D tenzor(npr. shape: [2])
        action_distribution = torch.distributions.Categorical(logits=logits)
        action = action_distribution.sample()
        taken_action_log_prob = log_probs.view(-1)[action]
        taken_action_log_probs.append(taken_action_log_prob)
        new_state, _, done, info = env.step(action.detach().data.numpy())
        state = torch.from_numpy(new_state).float()
        if done:
            episode_ended = True
            reward = -10
            env.reset()
        else:
            reward = 1.0
            # posto smo izvrsili N_steps koraka za poslednju nagradu uzimamo nasu predvidjenu q_value(bootstraping)
            G = q_value.detach()
        rewards.append(reward)
    return values, taken_action_log_probs, rewards, G, episode_ended

# chapter: 5, page: 134
def update_params(optimizer, values, taken_action_log_probs, rewards, G, clc=0.1, gamma=0.95):
    rewards = torch.Tensor(rewards).flip(dims=(0,)).view(-1)
    taken_action_log_probs = torch.stack(taken_action_log_probs).flip(dims=(0,)).view(-1)
    values = torch.stack(values).flip(dims=(0,)).view(-1)
    # flipovali smo ih sve jer zelimo da najskorije akcije najvise uzmemo u obzir
    Returns = []
    ret_ = G
    for r in range(rewards.shape[0]):
        ret_ = rewards[r] + gamma * ret_
        Returns.append(ret_)
    Returns = torch.stack(Returns).view(-1) # view(-1) -> da bismo bili sigurni da je 1D tenzor(npr. shape: [20])
    Returns = F.normalize(Returns, dim=0)
    # moramo pozvati detach() fn da ne bismo propagirali unazad i kroz actor head i critic head, jer mi zelimo samo da update-ujemo actor head, page: 130 pogledaj za vise
    actor_loss = -1*taken_action_log_probs * (Returns - values.detach())
    # ovde ne treba da pozovemo detach() fn, jer zelimo da update-ujemo critic head, page: 130 pogledaj za vise
    critic_loss = torch.pow(values - Returns, 2)
    # skaliramo critic loss tako sto ga mnozimo sa 0.1, jer zelimo da aktor uci brze od kriticara
    loss = actor_loss.sum() + clc*critic_loss.sum()
    loss.backward()
    optimizer.step()
    return actor_loss, critic_loss, len(rewards)

# chapter: 5, page: 134
def worker(process_id, model, counter, params, score):
    env = gym.make('CartPole-v1')
    env.reset()
    optimizer = optim.Adam(params=model.parameters(), lr=1e-4)
    optimizer.zero_grad()

    ep_len = 0
    ep_num = 0
    while (ep_num < params['epochs']):
        optimizer.zero_grad()
        values, log_probs, rewards, G, episode_ended = run_episode(env, model)
        actor_loss, critic_loss, episode_length = update_params(optimizer, values, log_probs, rewards, G)

        ep_len += episode_length
        if episode_ended:
            ep_num += 1
            score[counter.value] = ep_len
            counter.value = counter.value + 1
            ep_len = 0

def running_mean(x, N=50):
    if len(x) > N:
        kernel = np.ones(N)
        conv_len = x.shape[0]-N
        y = np.zeros(conv_len)
        for i in range(conv_len):
            y[i] = kernel @ x[i:i+N]
            y[i] /= N
        return y
    return x

def test_model(model, num_of_episodes=200):
    print('testing model...')

    env = gym.make('CartPole-v1')
    score = []

    for _ in range(num_of_episodes):
        done = False
        env.reset()
        j = 0
        state = torch.from_numpy(env.env.state).float()

        while (done == False):
            j += 1

            log_probs, q_value = model(state)
            action_distribution = torch.distributions.Categorical(logits=log_probs.view(-1))
            action = action_distribution.sample()
            new_state, reward, done, info = env.step(action.detach().data.numpy())
            state = torch.from_numpy(new_state).float()

        score.append(j)
    
    return score

if __name__ == '__main__':
    with Manager() as manager:
        # distribuirano treniranje
        MasterNode = ActorCritic()
        # f-ja share_memory() ce dozvoliti da parametri modela budu deljeni izmedju vise procesa(multiprocessing)
        MasterNode.share_memory()
        processes = []
        params = {
            'epochs': 1000,
            # cpu_count() returns the number of CPUs in the system
            # 'n_workers': mp.cpu_count() - 1
            # 'n_workers': 1
            'n_workers': 7
        }
        # print(mp.cpu_count())
        num_of_episodes = params['n_workers']*params['epochs']
        print('should train on:', num_of_episodes, 'episodes')

        # chapter: 5, page: 128
        # deljeni globalni brojac koji koristi ugradjeni deljeni objekat iz paketa multiprocessing, 'i' govori da je tip integer
        counter = mp.Value('i', 0)
        score = manager.list(range(num_of_episodes))
        # score = mp.Array('i', range(params['n_workers']*params['epochs']))
        for i in range(params['n_workers']):
            p = mp.Process(target=worker, args=(i, MasterNode, counter, params, score))
            p.start()
            processes.append(p)
        for p in processes:
            # "Joins" each process to wait for it to finish before returning to the main process
            p.join()
        for p in processes:
            # terminiramo svaki postojeci proces
            p.terminate()

        # print(score[0:5])
        print(counter.value, processes[0].exitcode)

        # score = test_model(MasterNode)
        fig = plt.figure(figsize=(10, 5))
        ax = fig.add_subplot(1, 1, 1)
        ax.title.set_text('Distributed advantage Actor Critic (DA2C) N-step')
        ax.set_xlabel("Training Epochs", fontsize=22)
        ax.set_ylabel("Episode Duration", fontsize=22)
        ax.plot(running_mean(np.array(score), len(score)//50), color='green')

        plt.tight_layout()
        plt.show()
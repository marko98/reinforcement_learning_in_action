import torch
import numpy as np
import random
from collections import deque
from matplotlib import pyplot as plt
import copy
from Gridworld import Gridworld

agent = torch.nn.Sequential(
    torch.nn.Linear(64, 150),
    torch.nn.ReLU(),
    torch.nn.Linear(150, 100),
    torch.nn.ReLU(),
    torch.nn.Linear(100, 4),
)
target_agent = copy.deepcopy(agent)
target_agent.load_state_dict(agent.state_dict())
lr = 1e-3
optimizer = torch.optim.Adam(params=agent.parameters(), lr=lr)
loss_fn = torch.nn.MSELoss()

epsilon = 1.0
gamma = 0.9

action_set = {
    0: 'u',
    1: 'd',
    2: 'l',
    3: 'r'
}
episodes = 5000
memory_size = 1000
batch_size = 200
experience_replay = deque(maxlen=memory_size)
losses = []
max_moves = 50
target_agent_update_step = 50
counter = 0
for episode in range(episodes):
    game = Gridworld(4, 'random')
    state_np = game.board.render_np().reshape(1, 64) + np.random.rand(1, 64) / 10.0
    status = 1
    move = 0
    losses.append({'loss': 0, 'episode': episode})
    while status == 1:
        counter += 1
        move += 1
        state = torch.Tensor(state_np).float()
        q_values = agent(state)
        q_values_np = q_values.data.numpy()
        if random.random() < epsilon:
            action_key = np.random.randint(0, 4)
        else:
            action_key = np.argmax(q_values_np)
        action = action_set[action_key]
        game.makeMove(action)
        reward = game.reward()
        done = True if reward != -1 else False
        new_state_np = game.board.render_np().reshape(1, 64) + np.random.rand(1, 64) / 10.0
        new_state = torch.Tensor(new_state_np).float()
        experience = (state, action_key, reward, new_state, done)
        experience_replay.append(experience)

        if len(experience_replay) > batch_size:
            minibatch = random.sample(experience_replay, batch_size)
            states = torch.cat([s for (s, ak, r, ns, d) in minibatch])
            new_states = torch.cat([ns for (s, ak, r, ns, d) in minibatch])
            action_keys = torch.Tensor([ak for (s, ak, r, ns, d) in minibatch])
            rewards = torch.Tensor([r for (s, ak, r, ns, d) in minibatch])
            done = torch.Tensor([d for (s, ak, r, ns, d) in minibatch])

            Q1 = agent(states)
            with torch.no_grad():
                Q2 = target_agent(new_states)

            Y = rewards + gamma * torch.max(Q2.detach(), dim=1)[0]  * (1 - done)
            index = action_keys.unsqueeze(dim=1).long()
            X = torch.gather(Q1, dim=1, index=index).squeeze(dim=1)
            optimizer.zero_grad()
            loss = loss_fn(X, Y)
            losses[-1]['loss'] += loss.item()
            loss.backward()
            optimizer.step()

            if counter % target_agent_update_step == 0:
                target_agent.load_state_dict(agent.state_dict())
        
        state_np = new_state_np

        if reward != -1 or move > max_moves:
            status = 0

    if epsilon > 0.1:
        epsilon -= 1/episodes

fig = plt.figure(figsize=(10, 5))
ax = fig.add_subplot(1, 1, 1)
ax.title.set_text('Agent learns to play Gridworld - DQN & experience replay & target agent')
ax.set_xlabel("Episodes")
ax.set_ylabel("Loss")
# ax.bar(np.arange(0, epochs), list(map(lambda x: x['loss'], losses)), color="b")
ax.bar(list(map(lambda x: x['episode'], losses)), list(map(lambda x: x['loss'], losses)), color="#89083A")

plt.tight_layout()
plt.show()

from test_model import test_model

# test_model(agent, 'random')

# chapter 3, page: 79
max_games = 1000
wins = 0
for i in range(max_games):
    win = test_model(target_agent, mode='random', display=False)
    if win:
        wins += 1
win_perc = float(wins) / float(max_games)
print("Games played: {0}, # of wins: {1}".format(max_games,wins))
print("Win percentage: {}%".format(100.0*win_perc))
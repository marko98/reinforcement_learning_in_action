import torch
import random
import numpy as np
import matplotlib.pyplot as plt

agent = torch.nn.Sequential(
    torch.nn.Linear(64, 150),
    torch.nn.ReLU(),
    torch.nn.Linear(150, 50),
    torch.nn.ReLU(),
    torch.nn.Linear(50, 4)
)
lr = 1e-3
optimizer = torch.optim.Adam(params=agent.parameters(), lr=lr)
loss_fn = torch.nn.MSELoss()

from Gridworld import Gridworld

action_set = {
    0: 'u',
    1: 'd',
    2: 'l',
    3: 'r'
}

episodes = 1000
losses = []
epsilon = 1.0
gamma = 0.9

for episode in range(1, episodes+1):

    game = Gridworld(4, 'static')
    state_np = game.board.render_np().reshape(1, 64) + np.random.rand(1, 64) / 10.0
    status = 1
    losses.append({'loss': 0, 'episode': episode})
    while status == 1:
        state = torch.Tensor(state_np).float()
        q_values = agent(state)
        q_values_np = q_values.data.numpy()

        if random.random() < epsilon:
            action_key = random.choice(list(action_set))
        else:
            action_key = np.argmax(q_values_np)

        action = action_set[action_key]
        game.makeMove(action)
        reward = game.reward()
        new_state_np = game.board.render_np().reshape(1, 64) + np.random.rand(1, 64) / 10.0
        with torch.no_grad():
            q_values_new_state = agent(torch.Tensor(new_state_np).float())
        q_values_new_state_np = q_values_new_state.detach().data.numpy()
        expected_reward_new_state = np.ndarray.max(q_values_new_state_np)

        if reward == -1:
            y = reward + gamma * expected_reward_new_state
        else:
            y = reward
            status = 0

        x = q_values.squeeze()[action_key]
        y = torch.Tensor([y]).float().squeeze()
        optimizer.zero_grad()
        loss = loss_fn(x, y)
        losses[-1]['loss'] += loss.item()
        loss.backward()
        optimizer.step()

        state_np = new_state_np

    if epsilon > 0.1:
        epsilon -= 1/episodes

fig = plt.figure(figsize=(10, 5))
ax = fig.add_subplot(1, 1, 1)
ax.title.set_text('Agent learns to play Gridworld - DQN')
ax.set_xlabel("Episodes")
ax.set_ylabel("Loss")
# ax.bar(np.arange(0, epochs), list(map(lambda x: x['loss'], losses)), color="b")
ax.bar(list(map(lambda x: x['episode'], losses)), list(map(lambda x: x['loss'], losses)), color="#89083A")

plt.tight_layout()
plt.show()

from test_model import test_model

test_model(agent, 'static')
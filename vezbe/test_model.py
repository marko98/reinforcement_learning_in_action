import numpy as np
import torch
import random

from Gridworld import Gridworld

action_set = {
    0: 'u',
    1: 'd',
    2: 'l',
    3: 'r'
}

# testing
def test_model(model, mode='static', display=True):
    move_counter = 0
    test_game = Gridworld(size=4, mode=mode)
    state = test_game.board.render_np().reshape(1, 64) + np.random.rand(1, 64) / 10.0
    state = torch.from_numpy(state).float()
    if display:
        print('Initial State: \n{}'.format(test_game.display()))
    status = 1
    while status == 1:
        q_values = model(state)
        q_values_np = q_values.data.numpy()
        action_key = np.argmax(q_values_np)
        action = action_set[action_key]
        if display:
            print('Move counter #: %s; Taking action: %s' % (move_counter, action))
        test_game.makeMove(action)
        state = test_game.board.render_np().reshape(1, 64) + np.random.rand(1, 64) / 10.0
        state = torch.from_numpy(state).float()
        if display:
            print(test_game.display())
        reward = test_game.reward()

        if reward != -1:
            if reward > 0:
                status = 2
                if display:
                    print('Game won! Reward: %s' % (reward,))
            else:
                status = 0
                if display:
                    print('Game LOST! Reward: %s' % (reward,))

        move_counter += 1

        if move_counter > 15:
            if display:
                print('Game lost; too many moves.')
            break

    win = True if status == 2 else False
    return win
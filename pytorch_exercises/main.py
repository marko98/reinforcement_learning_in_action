import torch

t = torch.Tensor([[0,1,2,3,4,5,6,7,8,9], [10,11,12,13,14,15,16,17,18,19], [20,21,22,23,24,25,26,27,28,29], [30,31,32,33,34,35,36,37,38,39]]).int()
print(t, "\n")

# squeeze, unsqueeze
indices = torch.LongTensor([3,7,4,1]).unsqueeze(1)
print(indices, "\n")
print(indices.squeeze(1), "\n")

# gather
print(torch.gather(t, 1, indices), "\n")

# cat 
t = torch.cat([t for t in [torch.Tensor([1,2,3]).int(), torch.Tensor([4,5,6]).int()]], dim=0)
# t = torch.cat([t for t in [torch.Tensor([1,2,3]).unsqueeze(1).int(), torch.Tensor([4,5,6]).unsqueeze(1).int()]], dim=1)
# t = torch.cat([t for t in [torch.Tensor([[1],[2],[3]]).int(), torch.Tensor([[4],[5],[6]]).int()]], dim=1)

print(t, "\n")

t = torch.vstack([t for t in [torch.Tensor([1,2,3]).int(), torch.Tensor([4,5,6]).int()]])
# t = torch.hstack([t for t in [torch.Tensor([1,2,3]).int(), torch.Tensor([4,5,6]).int()]])
print(t, "\n")

print(torch.Tensor([5, 4]).shape, "\n")

print(1-torch.Tensor([0.,0.,0.]), "\n")

print(torch.max(torch.Tensor([[4, 5], [1, 8]]), dim=1), "\n")
print(torch.max(torch.Tensor([[4, 5], [1, 8]]), dim=1)[0], "\n")

print(torch.max(torch.Tensor([2, 7, 9]), dim=0)[0], "\n")
print(torch.Tensor([2, 7, 9]).max(), "\n")
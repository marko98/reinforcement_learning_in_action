import gym
from gym import envs
import torch
import numpy as np
import matplotlib.pyplot as plt

# # list of environments
# print(envs.registry.all())

# env = gym.make('CartPole-v0')

# state1 = env.reset()
# action = env.action_space.sample()
# state, reward, done, info = env.step(action)

# chapter: 4, page: 104

l1 = 4
l2 = 150
l3 = 2

model = torch.nn.Sequential(
    torch.nn.Linear(l1, l2),
    torch.nn.LeakyReLU(),
    torch.nn.Linear(l2, l3),
    torch.nn.Softmax(dim=0)
)

learning_rate = 0.009
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

def discount_rewards(rewards, gamma = 0.99):
    lenr = len(rewards)
    discounted_rewards = torch.pow(gamma, torch.arange(lenr).float()) * rewards
    # discounted_rewards /= discounted_rewards.max()
    discounted_rewards /= torch.max(discounted_rewards, dim=0)[0]
    return discounted_rewards

# disc_rewards = discount_rewards(torch.Tensor([1, 2, 3]).float().flip(dims=(0,)))
# print(disc_rewards)

def loss_fn(preds, r):
    return -1 * torch.sum(r * torch.log(preds))

MAX_DUR = 200
MAX_EPISODES = 500
gamma = 0.99
score = []
env = gym.make('CartPole-v0')
show_screen = False
for episode in range(MAX_EPISODES):
    curr_state = env.reset()
    done = False
    transitions = []

    for time_step in range(MAX_DUR):
        curr_state = torch.from_numpy(curr_state).float()
        actions_probabilities = model(curr_state)
        action = np.random.choice(np.array([0, 1]), p=actions_probabilities.data.numpy())
        prev_state = curr_state
        curr_state, reward, done, info = env.step(action)
        if show_screen:
            env.render()

        if len(transitions) > 0:
            reward += transitions[-1][2]
        transitions.append((prev_state.data.numpy(), action, reward))

        if done:
            if show_screen:
                print('game over, episode:', episode, ', episode lasted:', time_step)
            break
    
    episode_length = len(transitions)
    score.append(episode_length)
    # obrcemo redosled nagrada jer smatramo da je poslednja akcija ona koja gubi epizodu i nju zelimo najmanje da nagradimo, odnosno da je najmanje 'ohrabrimo'
    reward_batch = torch.Tensor([r for (s, a, r) in transitions]).flip(dims=(0,))
    disc_rewards = discount_rewards(reward_batch, gamma)
    state_batch = torch.Tensor([s for (s, a, r) in transitions])
    action_batch = torch.Tensor([a for (s, a, r) in transitions])
    prob_batch = model(state_batch)

    # a = action_batch.unsqueeze(dim=1)
    prob_batch_for_taken_actions = torch.gather(prob_batch, 1, action_batch.unsqueeze(dim=1).long()).squeeze(dim=1)
    
    # loss = loss_fn(prob_batch_for_taken_actions, disc_rewards)

    # loss fn u delovima:
    # https://pytorch.org/docs/stable/generated/torch.log.html
    # torch.log() fn nam vraca novi tenzor cije su vrednosti elemenata natural logarithm vrednosti elemenata prethodnog tenzora
    # primenjujemo natural logarithm nad svakim clanom raspodele verovatnoca preduzetih akcija da bi smo ih sveli otprilike na raspon 
    # izmedju 0 i 1 ili -1 i 0(ukoliko su vrednosti u raspodeli izmedju 0 i 1)
    prob_batch_for_taken_actions_log_neg = torch.log(prob_batch_for_taken_actions)
    # buduci da su vrednosti u raspodeli uglavnom izmedju 0 i 1, rezultati primenjenih log fns su negativni i zato ih
    # mnozimo sa tenzorom istog oblika(shape tj size) kao i prob_batch_for_taken_actions_log, sa elementima -1 da bi obrnuli znak
    # prob_batch_for_taken_actions_log_pos = (-1*torch.ones(prob_batch_for_taken_actions_log_neg.size())) * prob_batch_for_taken_actions_log_neg
    prob_batch_for_taken_actions_log_pos = -1 * prob_batch_for_taken_actions_log_neg
    y = disc_rewards * prob_batch_for_taken_actions_log_pos
    # sumiramo jer propagacija unazad moze da se izvede samo od skalara
    loss = torch.sum(y)

    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

def running_mean(x, N=50):
    kernel = np.ones(N)
    conv_len = x.shape[0]-N
    y = np.zeros(conv_len)
    for i in range(conv_len):
        y[i] = kernel @ x[i:i+N]
        y[i] /= N
    return y

fig = plt.figure(figsize=(10, 5))
ax = fig.add_subplot(1, 1, 1)
ax.title.set_text('Agent learns to play CartPole - REINFORCE algorithm')
ax.set_xlabel("Training Epochs",fontsize=22)
ax.set_ylabel("Episode Duration", fontsize=22)
ax.plot(running_mean(np.array(score), 50), color='green')

plt.tight_layout()
plt.show()
import numpy as np

# chapter: 7, page: 177
probs = np.array([0.6, 0.1, 0.1, 0.1, 0.1])
support = np.array([18, 21, 17, 17, 21])

def get_expected_value(support, probs):
    # expected_value = 0
    # for i in range(probs.shape[0]):
    #     expected_value += probs[i] * support[i]
    # return expected_value
    return probs @ support # mnozenje vektora, matrica

# print('weighted average(expected_value):', get_expected_value(support, probs))

# chapter: 7, page: 184
# discrete probability distribution
import torch
from matplotlib import pyplot as plt

vmin, vmax = -10, 10
number_of_support_elements = 51
support = np.linspace(vmin, vmax, number_of_support_elements)
probs = np.ones(number_of_support_elements)
probs /= probs.sum()
# plt.bar(support, probs)
# plt.show()

# chapter: 7, page: 187
# updating probability distribution
def update_dist(reward, support, probs, lim=(-10, 10), gamma=0.8):
    number_of_support_elements = probs.shape[0]
    vmin, vmax = lim[0], lim[1]
    dz = (vmax-vmin)/(number_of_support_elements-1.)
    bj = np.round((reward-vmin)/dz)
    bj = int(np.clip(bj, 0, number_of_support_elements - 1))
    m = probs.clone()
    j = 1
    for i in range(bj, 1, -1):
        m[i] += np.power(gamma, j) * m[i-1]
        j += 1
    j = 1
    for i in range(bj, number_of_support_elements-1, 1):
        m[i] += np.power(gamma, j) * m[i+1]
        j += 1
    m /= m.sum()
    return m

# # chapter: 7, page: 189
# observed_reward = -1
# # Z - value distribution
# Z = torch.from_numpy(probs).float()
# Z = update_dist(observed_reward, torch.from_numpy(support).float(), Z, lim=(vmin, vmax), gamma=0.1)
# plt.bar(support, Z)
# plt.show()

# observed_rewards = [10, 10, 10, 0, 1, 0, -10, -10, 10, 10]
# # Z - value distribution
# Z = torch.from_numpy(probs).float()
# for i in range(len(observed_rewards)):
#     Z = update_dist(observed_rewards[i], torch.from_numpy(support).float(), Z, lim=(vmin, vmax), gamma=0.5)
# plt.bar(support, Z)
# plt.show()

# # chapter: 7, page: 190
observed_rewards = [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5]
# Z - value distribution
Z = torch.from_numpy(probs).float()
for i in range(len(observed_rewards)):
    Z = update_dist(observed_rewards[i], torch.from_numpy(support).float(), Z, lim=(vmin, vmax), gamma=0.7)
plt.bar(support, Z)
plt.show()
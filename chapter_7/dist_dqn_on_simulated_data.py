import torch
import numpy as np
from matplotlib import pyplot as plt

# chapter: 7, page: 187
# updating probability distribution
def update_dist(reward, support, probs, lim=(-10, 10), gamma=0.8):
    number_of_support_elements = probs.shape[0]
    vmin, vmax = lim[0], lim[1]
    dz = (vmax-vmin)/(number_of_support_elements-1.)
    bj = np.round((reward-vmin)/dz)
    bj = int(np.clip(bj, 0, number_of_support_elements - 1))
    m = probs.clone()
    j = 1
    for i in range(bj, 1, -1):
        m[i] += np.power(gamma, j) * m[i-1]
        j += 1
    j = 1
    for i in range(bj, number_of_support_elements-1, 1):
        m[i] += np.power(gamma, j) * m[i+1]
        j += 1
    m /= m.sum()
    return m

# chapter: 7, page: 192
def distributional_dqn(state, parameters_vector, action_space=3):
    dim0, dim1, dim2, dim3 = 128, 100, 25, 51
    l1_weights = parameters_vector[0:dim0*dim1].reshape(dim0, dim1)
    l2_weights = parameters_vector[dim0*dim1:dim0*dim1+dim1*dim2].reshape(dim1, dim2)
    l1 = state @ l1_weights
    l1 = torch.selu(l1) #https://pytorch.org/docs/stable/generated/torch.nn.SELU.html
    l2 = l1 @ l2_weights
    l2 = torch.selu(l2)
    l3 = []
    for i in range(action_space):
        step = dim2*dim3
        l3__weights = parameters_vector[dim0*dim1+dim1*dim2+i*step:dim0*dim1+dim1*dim2+i*step+step].reshape(dim2, dim3)
        l3_ = l2 @ l3__weights
        l3_ = torch.selu(l3_)
        l3.append(l3_)
    l3 = torch.stack(l3, dim=1)
    # https://pytorch.org/docs/stable/nn.functional.html#torch.nn.functional.softmax
    l3 = torch.nn.functional.softmax(l3, dim=2)
    # l3 = l3.squeeze()
    return l3

# chapter: 7, page: 193
def get_target_distribution(distribution_batch, action_batch, reward_batch, support, lim=(-10, 10), gamma=0.8):
    number_of_support_elements = support.shape[0]
    vmin, vmax = lim[0], lim[1]
    dz = (vmax-vmin)/(number_of_support_elements-1.)
    target_distribution_batch = distribution_batch.clone()

    for batch in range(distribution_batch.shape[0]):
        distribution_full = distribution_batch[batch]
        action = int(action_batch[batch].item())
        distribution = distribution_full[action]
        reward = reward_batch[batch]
        if reward != -1:
            # degenerate distribution
            target_distribution = torch.zeros(number_of_support_elements)
            bj = np.round((reward-vmin)/dz)
            bj = int(np.clip(bj, 0, number_of_support_elements-1))
            target_distribution[bj] = 1.0
        else:
            target_distribution = update_dist(reward, support, distribution, lim=lim, gamma=gamma)
        target_distribution_batch[batch,action] = target_distribution

    return target_distribution_batch

# chapter: 7, page: 198
def loss_fn(pred_distribution_batch, target_distribution_batch):
    loss = torch.Tensor([0.])
    loss.requires_grad = True
    for batch in range(pred_distribution_batch.shape[0]):
        # za squeeze moras imate shape sa najmanje 3 dimenzije
        # flattened = pred_distribution_batch[batch].flatten(start_dim=0)
        loss_ = -1 * torch.log(pred_distribution_batch[batch].flatten(start_dim=0) @ target_distribution_batch[batch].flatten(start_dim=0))
        loss = loss + loss_
    return loss

# action_space = 3
# number_of_support_elements = 51
# size = 128*100 + 100*25 + action_space*25*number_of_support_elements
# # delimo sa 2.0 jer torch.randn() je fn koja vraca array sa elementima cije su vrednosti u rasponu koji 
# # ima normalna raspodela sa mean 0 i varijansom 1
# # https://homepage.divms.uiowa.edu/~mbognar/applets/normal.html
# parameters_vector = torch.randn(size) / 10.0 # / 2.0, moze i sa vecim brojem od 2.0
# # print(parameters_vector.requires_grad)
# parameters_vector.requires_grad = True
# vmin, vmax = -10, 10
# gamma = 0.9
# lr = 0.00001
# update_rate = 75 # num of steps for syncronising target dist-dqn with main dist-dqn
# support = np.linspace(vmin, vmax, number_of_support_elements)
# state = torch.randn(2, 128) / 10.0
# action_batch = torch.Tensor([0, 2])
# reward_batch = torch.Tensor([0, 10])
# losses = []
# pred_dist_batch = distributional_dqn(state, parameters_vector, action_space)
# target_dist_batch = get_target_distribution(pred_dist_batch, action_batch, reward_batch, support, lim=(vmin, vmax), gamma=gamma)
# # loss_fn(pred_dist_batch, target_dist_batch)

# # s = target_dist_batch.flatten(start_dim=1)
# plt.plot((target_dist_batch.flatten(start_dim=1)[0].data.numpy()),color='red',label='target')
# plt.plot((pred_dist_batch.flatten(start_dim=1)[0].data.numpy()),color='green',label='pred')
# plt.legend()
# plt.show()

# chapter: 7, page: 200
action_space = 3
number_of_support_elements = 51
size = 128*100 + 100*25 + action_space*25*number_of_support_elements
# delimo sa 2.0 jer torch.randn() je fn koja vraca array sa elementima cije su vrednosti u rasponu koji 
# ima normalna raspodela sa mean 0 i varijansom 1
# https://homepage.divms.uiowa.edu/~mbognar/applets/normal.html
parameters_vector = torch.randn(size) / 10.0 # / 2.0, moze i sa vecim brojem od 2.0
# print(parameters_vector.requires_grad)
parameters_vector.requires_grad = True
# parameters_vector2 acts as target agent
parameters_vector2 = parameters_vector.detach().clone()
vmin, vmax = -10, 10
gamma = 0.9
lr = 0.00001
update_rate = 75 # num of steps for syncronising target dist-dqn with main dist-dqn
support = torch.linspace(vmin, vmax, number_of_support_elements)
state = torch.randn(2, 128) / 10.0
action_batch = torch.Tensor([0, 2])
losses = []
for i in range(1000):
    reward_batch = torch.Tensor([0, 8]) + torch.randn(2) / 10.0
    pred_dist_batch = distributional_dqn(state, parameters_vector, action_space)

    pred_dist_batch2 = distributional_dqn(state, parameters_vector2, action_space)
    target_dist_batch = get_target_distribution(pred_dist_batch2, action_batch, reward_batch, support, lim=(vmin, vmax), gamma=gamma)

    loss = loss_fn(pred_dist_batch, target_dist_batch.detach())
    losses.append(loss.item())
    loss.backward()
    # Gradient Descent
    with torch.no_grad():
        parameters_vector -= lr * parameters_vector.grad
    parameters_vector.requires_grad = True

    if i % update_rate == 0:
        parameters_vector2 = parameters_vector.detach().clone()

fig, ax = plt.subplots(1, 2)
ax[0].plot(target_dist_batch.flatten(start_dim=1)[0].data.numpy(), color='red', label='target')
ax[0].plot(pred_dist_batch.flatten(start_dim=1)[0].data.numpy(), color='green', label='pred')
ax[0].legend()
# (page: 201) razlog zasto ces na grafu videti da greska opada ali ima trenutke kada skoci pa padne je zbog 
# sinhronizacije target mreze(parametara) sa online mrezom(parametrima) jer neposredno nakon sto se sinhronizuju
# target_dist_batch bude dosta drugaciji pa greska bude veca ali odmah i opadne
ax[1].plot(losses)
plt.show()

# chapter: 7, page: 202
tpred = pred_dist_batch
cs = ['gray','green','red']
num_batch = 2
labels = ['Action {}'.format(i,) for i in range(action_space)]
fig,ax = plt.subplots(nrows=num_batch,ncols=action_space)

for j in range(num_batch): #A 
    for i in range(tpred.shape[1]): #B
        ax[j,i].bar(support.data.numpy(),tpred[j,i,:].data.numpy(),\
                label='Action {}'.format(i),alpha=0.9,color=cs[i])
        ax[j,i].legend()
plt.show()
import gym
from collections import deque
import torch
import numpy as np
import random
from matplotlib import pyplot as plt
import skvideo
# https://blog.gregzaal.com/how-to-install-ffmpeg-on-windows/
# https://www.gyan.dev/ffmpeg/builds/
skvideo.setFFmpegPath('C:/ffmpeg/bin')
import skvideo.io
from skimage.transform import resize
from skimage import img_as_ubyte
import json
from datetime import datetime

# chapter: 7, page: 192
def distributional_dqn_forward(state, parameters_vector, action_space):
    dim0, dim1, dim2, dim3 = 128, 100, 25, 51

    l1_weights = parameters_vector[0:dim0*dim1].reshape(dim0, dim1)
    l2_weights = parameters_vector[dim0*dim1:dim0*dim1+dim1*dim2].reshape(dim1, dim2)

    l1 = state @ l1_weights
    l1 = torch.nn.functional.selu(l1)
    l2 = l1 @ l2_weights
    l2 = torch.nn.functional.selu(l2)

    l3 = []
    for i in range(action_space):
        step = dim2*dim3
        l3__weights = parameters_vector[dim0*dim1+dim1*dim2+i*step:dim0*dim1+dim1*dim2+i*step+step].reshape(dim2, dim3)
        l3_ = l2 @ l3__weights
        l3.append(l3_)
    l3 = torch.stack(l3, dim=1)
    l3 = torch.nn.functional.softmax(l3, dim=2)
    # l3.squeeze()
    return l3

# chapter: 7, page: 187
def update_distribution(reward, support, probabilities, lim=(-10, 10), gamma=0.8):
    number_of_support_elements = probabilities.shape[0]
    vmin, vmax = lim[0], lim[1]
    dz = (vmax-vmin)/(number_of_support_elements-1)
    bj = np.round((reward-vmin)/dz)
    bj = int(np.clip(bj, 0, number_of_support_elements-1))
    m = probabilities.clone()
    j = 1
    for i in range(bj, 1, -1):
        m[i] += np.power(gamma, j) * m[i-1]
        j += 1
    j = 1
    for i in range(bj, number_of_support_elements-1, 1):
        m[i] += np.power(gamma, j) * m[i+1]
        j += 1
    m /= m.sum()
    return m

# chapter: 7, page: 193
def get_target_distribution_batch(pred_dist_batch, action_batch, reward_batch, support, lim=(-10, 10), gamma=0.8):
    number_of_support_elements = support.shape[0]
    vmin, vmax = lim[0], lim[1]
    dz = (vmax-vmin)/(number_of_support_elements-1)
    target_dist_batch = pred_dist_batch.clone()
    for batch in range(pred_dist_batch.shape[0]):
        distribution_full = pred_dist_batch[batch]
        action = int(action_batch[batch].item())
        distribution = distribution_full[action]
        reward = reward_batch[batch]
        if reward != -1:
            target_distribution = torch.zeros(number_of_support_elements)
            bj = np.round((reward-vmin)/dz)
            bj = int(np.clip(bj, 0, number_of_support_elements-1))
            target_distribution[bj] = 1.
        else:
            target_distribution = update_distribution(reward, support, distribution, lim, gamma)
        target_dist_batch[batch][action] = target_distribution
        # target_dist_batch[batch, action] = target_distribution
    return target_dist_batch

# chapter: 7, page: 198
def loss_fn(pred_dist_batch, target_dist_batch):
    loss = torch.Tensor([0.])
    loss.requires_grad = True
    for batch in range(pred_dist_batch.shape[0]):
        loss_ = -1 * target_dist_batch[batch].flatten(start_dim=0) @ torch.log(pred_dist_batch[batch].flatten(start_dim=0))
        loss = loss + loss_
    return loss

# chapter: 7, page: 203
def preprocess_state(state):
    # unsqueeze(dim=0) radimo to da bismo kreirali kao state_batch 80%
    p_state = torch.from_numpy(state).unsqueeze(dim=0).float()
    p_state = torch.nn.functional.normalize(p_state, dim=1)
    return p_state

def get_action_batch(distribution_batch, support):
    actions = []
    for batch in range(distribution_batch.shape[0]):
        distribution_full = distribution_batch[batch]
        expected_values = [support @ distribution_full[action] for action in range(distribution_batch.shape[1])]
        action = int(np.argmax(expected_values))
        actions.append(action)
    actions = torch.Tensor(actions).int()
    return actions

# https://github.com/DeepReinforcementLearning/DeepReinforcementLearningInAction/blob/master/old_but_more_detailed/Ch7_DistDQN.ipynb
def get_dist_plot(env, dist, support, shape=(105*2, 80*2, 3)):
    """
    This function renders a side-by-side RGB image (returned as numpy array)
    of the current environment state (left) next to the current predicted probability distribution
    over rewards.
    
    `env` OpenAI Gym instantiated environment object
    `dist`: A x 51 tensor , where `A` is action space dimension
    `support` vector of supports
    `shape` (RGB: width,height,channels) desired output image size
    
    Output:
    - numpy array of RGB image (w,ht,ch)
    """
    cs = ['cyan','yellow','red','green','magenta']
    fig, ax = plt.subplots(1,1)
    fig.set_size_inches(5,5)
    for action in range(dist.shape[0]): #loop through actions
        _ = ax.bar(support.data.numpy(), dist[action].data.numpy(), label='{}'.format(env.env.get_action_meanings()[action]), alpha=0.9, color=cs[action])
    ax.get_yaxis().set_visible(False)
    support_ = np.linspace(support.min(), support.max(), 5)
    ax.set_xticks(support_)
    ax.set_xticklabels(support_)
    ax.xaxis.label.set_color('white')
    ax.tick_params(axis='both', which='major', labelsize=20)
    ax.tick_params(axis='x', colors='white')
    fig.set_facecolor('black')
    ax.set_facecolor('black')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.legend()
    fig.legend(loc='upper left', prop={'size': 20})
    fig.canvas.draw()
    plt.close(fig)
    width, height = fig.get_size_inches() * fig.get_dpi()
    width,height=int(width),int(height)    
    image1 = np.fromstring(fig.canvas.tostring_rgb(), sep='', dtype='uint8').reshape(height, width, 3)
    image2 = resize(image1, shape)
    image2 = img_as_ubyte(image2)
    state_render = img_as_ubyte(resize(env.render('rgb_array'), shape))
    image3 = np.hstack((state_render, image2))
    # plt.imshow(image3)
    # plt.show()
    return image3

# chapter: 7, page: 204
env = gym.make('Freeway-ram-v0')
action_space = 3
print(env.env.get_action_meanings())

# data = {
#     'vmin': -10,
#     'vmax': 10,
#     'number_of_support_elements': 51,
#     'replay_memory_size': 200,
#     'batch_size': 50,
#     'priority_level': 5, #Prioritized-replay; duplicates highly informative experiences in the replay this many times
#     'lr': 0.0001,
#     'gamma': 0.1,
#     'epsilon': 0.20,
#     'epsilon_min': 0.05,
#     'update_target_agent_freq': 25,
#     'epochs': 1300
# }

# data = {
#     "vmin": -10,
#     "vmax": 10,
#     "number_of_support_elements": 51,
#     "replay_memory_size": 200,
#     "batch_size": 50,
#     "priority_level": 30, #Prioritized-replay; duplicates highly informative experiences in the replay this many times
#     "lr": 0.001,
#     "gamma": 0.05,
#     "epsilon": 0.2,
#     "epsilon_min": 0.05,
#     "update_target_agent_freq": 30,
#     "epochs": 1300,
# }

data = {
    "vmin": -10,
    "vmax": 10,
    "number_of_support_elements": 51,
    "replay_memory_size": 200,
    "batch_size": 50,
    "priority_level": 30, #Prioritized-replay; duplicates highly informative experiences in the replay this many times
    "lr": 0.0001,
    "gamma": 0.4,
    "epsilon": 0.2,
    "epsilon_min": 0.05,
    "update_target_agent_freq": 30,
    "epochs": 1300,
}

vmin, vmax = data['vmin'], data['vmax']
replay_memory_size = data['replay_memory_size']
batch_size = data['batch_size']
number_of_support_elements = data['number_of_support_elements']
dz = (vmax-vmin)/(number_of_support_elements-1)
support = torch.linspace(vmin, vmax, number_of_support_elements)

replay = deque(maxlen=replay_memory_size)
lr = data['lr']
gamma = data['gamma']
epochs = data['epochs']
epsilon = data['epsilon']
epsilon_min = data['epsilon_min']
priority_level = data['priority_level'] #Prioritized-replay; duplicates highly informative experiences in the replay this many times
update_freq = data['update_target_agent_freq']

# Initialize DQN parameters vector
parameters_size = 128*100 + 100*25 + action_space*25*number_of_support_elements
parameters_vector = torch.randn(parameters_size) / 10.0
parameters_vector.requires_grad = True
parameters_vector2 = parameters_vector.detach().clone() # paramteres for target network
losses = []
cum_rewards = [] # stores each win(successful freeway crossing) as a 1 in this list
renders = []
state = preprocess_state(env.reset())

# chapter: 7, page: 205
# main training loop
for epoch in range(epochs):
    pred_dist_batch = distributional_dqn_forward(state, parameters_vector, action_space)

    if epoch < replay_memory_size or np.random.rand(1) < epsilon:
        action = np.random.randint(action_space)
    else:
        # action = get_action_batch(pred_dist_batch.detach(), support)
        action = get_action_batch(pred_dist_batch.detach(), support).item()

    new_state, reward, done, info = env.step(action)
    new_state = preprocess_state(new_state)
    if reward == 1:
        cum_rewards.append(1)
    reward = 10 if reward == 1 else reward
    reward = -10 if done else reward
    reward = -1 if reward == 0 else reward
    experience = (state, action, reward, new_state)
    replay.append(experience)

    # if epoch % 5 == 0:
    #     render_image = get_dist_plot(env, pred_dist_batch.squeeze(dim=0), support)
    #     renders.append(render_image)

    if reward == 10:
        print("Won at epoch={}".format(epoch,))
        for _ in range(priority_level):
            replay.append(experience)

    random.shuffle(replay)
    state = new_state

    if epoch == replay_memory_size+1:
        print("Training started")
    if len(replay) == replay_memory_size:
        minibatch = random.sample(replay, batch_size)
        state_batch = torch.stack([s for (s, a, r, ns) in minibatch]).squeeze(dim=1)
        action_batch = torch.Tensor([a for (s, a, r, ns) in minibatch])
        reward_batch = torch.Tensor([r for (s, a, r, ns) in minibatch])
        new_state_batch = torch.stack([ns for (s, a, r, ns) in minibatch]).squeeze(dim=1)

        pred_dist_batch = distributional_dqn_forward(state_batch.detach(), parameters_vector, action_space)
        pred_dist2_batch = distributional_dqn_forward(new_state_batch.detach(), parameters_vector2, action_space)
        target_dist_batch = get_target_distribution_batch(pred_dist2_batch, action_batch, reward_batch, support, (vmin, vmax), gamma)

        loss = loss_fn(pred_dist_batch, target_dist_batch.detach())
        losses.append(loss.item())
        loss.backward()

        with torch.no_grad():
            parameters_vector -= lr * parameters_vector.grad
        parameters_vector.requires_grad = True
    
    if epoch % update_freq == 0:
        parameters_vector2 = parameters_vector.detach().clone()

    if epoch > 100 and epsilon > epsilon_min:
        dec = 1./np.log2(epoch)
        dec /= 1e3 #1000.0
        epsilon -= dec
    
    if done:
        state = preprocess_state(env.reset())
        done = False
        print("End of game.")

plt.plot(losses)
plt.show()
print('successful chicken crossings number:', len(cum_rewards))

#Stack generated frames into a movie
if len(renders) > 1:
    print('renders len:', len(renders))
    renders = np.stack(renders, axis=0)

    #Create mp4 video from rendered numpy frames
    skvideo.io.vwrite("./chapter_7/freeway_movies/freeway_epochs_{}.mp4".format(epochs,), renders)
    print("Done. Open video with VLC player")

# save parameters
with open('./chapter_7/freeway_params/params_epochs_{}_{}_csc_{}.json'.format(epochs, datetime.today().isoformat().replace(':', '-'), len(cum_rewards),), 'w') as outfile:
    data['agent_params'] = parameters_vector2.data.numpy().tolist(),
    data['successful_chicken_crossings_number'] = len(cum_rewards)
    json.dump(data, outfile)
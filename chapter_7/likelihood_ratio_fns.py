import numpy as np

# chapter: 7, page: 195
# likelihood: p/q

# recimo da je p lista verovatnoca dobijenih od mreze, a q lista verovatnoca iz stvarnog sveta

# # odkomentarisi da vidis sta se desava, mi zelimo da menjamo tezine(do backpropagation and gradient descent) da bi p za odredjen ulaz davao vrednosti slicne/iste q
# p = np.array([0.6, 0.5])
# q = np.array([0.6, 0.5])

p = np.array([0.4, 0.4])
q = np.array([0.6, 0.5])

# chapter: 7, page: 195
def likelihood_ratio(p, q):
    return np.prod(p/q)

print(p/q)
print(likelihood_ratio(p, q), '\n')


# chapter: 7, page: 196
def likelihood_ratio_2(p, q):
    # np.log() racuna prirodne logaritme od elemenata u nizu, buduci da su elementi ovde verovatnoce(tiny floating numbers) i zbog toga
    # da racunar ne bi racunao proizvod takvih brojeva sto bi dovelo do neke greske(jer racunari imaju odredjen opseg u kojem mogu prikazati brojeve)
    # primenicemo prirodni logaritam na elemente niza sto ce verovatnoce koje teze 0 svesti na velike brojeve koji teze -infinity i verovatnoce
    # koje teze 1 na brojeve koji teze 0

    # dodatno necemo mnoziti elemente u nizu kao u prethodnoj funkciji vec cemo ih sabirati sto je bolje za racunar
    # sabiranje je moguce jer vazi sledece: log(a*b) = log(a) + log(b)

    # tako da ova f-ja ce isto sracunati kao i prethodna samo ce biti tacnija i laksa za racunanje racunaru
    return np.sum(np.log(p/q))

print(np.log(p/q))
print(likelihood_ratio_2(p, q))
print('Calculate the exponential(jer je to suprotno od prirodnog logaritma) of all elements in the input array:', np.exp(likelihood_ratio_2(p, q)), '\n')


def likelihood_ratio_3(p, q):
    # slicna f-ja prethodnoj f-ji jedino sto ovde vazi:
    # we can weight individual samples differently
    x = q * np.log(p/q)
    x = np.sum(x)
    return x

print(np.log(p/q))
print(likelihood_ratio_3(p, q))
print('Calculate the exponential(jer je to suprotno od prirodnog logaritma) of all elements in the input array:', np.exp(likelihood_ratio_3(p, q)), '\n')


# chapter: 7, page: 197
# we want high likelihood ratio to becomes a small error or loss
# Kullback-Leibler divergence - f-ja koja nam govori koliko su dve raspodele verovatnoca razlicite
def likelihood_ratio_loss_fn(p, q):
    # slicna f-ja prethodnoj f-ji jedino sto ovde vazi:
    # we can weight individual samples differently
    x = q * np.log(p/q)
    x = np.sum(x)
    x *= -1
    return x

print(np.log(p/q))
print(likelihood_ratio_loss_fn(p, q))
print('loss:', np.exp(likelihood_ratio_loss_fn(p, q)), '\n')
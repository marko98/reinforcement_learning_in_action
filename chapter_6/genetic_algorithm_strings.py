import random
from matplotlib import pyplot as plt

# pod evolucione algoritme spadaju izmedju ostalog:
# - genetski algoritam(konkretno ova implementacija)
# - evolucione strategije
# itd

alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,.! "
# target = "Hello World!"
target = "David"

# chapter: 6, page: 148
class Individual:
    def __init__(self, string, fitness=0):
        self.string = string
        self.fitness = fitness

from difflib import SequenceMatcher

def similar(a, b):
    return SequenceMatcher(None, a, b).ratio() # float in [0,1]

def spawn_population(length=26, size=100):
    population = []
    for _ in range(size):
        string = ''.join(random.choices(alphabet, k=length))
        individual = Individual(string)
        population.append(individual)
    return population

def recombine(parent_1, parent_2):
    p1 = parent_1.string
    p2 = parent_2.string
    child_1 = []
    child_2 = []
    cross_pt = random.randint(0, len(p1))
    child_1.extend(p1[0:cross_pt])
    child_1.extend(p2[cross_pt:])
    child_2.extend(p2[0:cross_pt])
    child_2.extend(p1[cross_pt:])
    c1 = Individual(''.join(child_1))
    c2 = Individual(''.join(child_2))
    return c1, c2

# chapter: 6, page: 149
def mutate(individual, mutation_rate=0.01):
    individual_mutation_chars = []
    for char in individual.string:
        if random.random() < mutation_rate:
            individual_mutation_chars.append(random.choice(alphabet))
        else:
            individual_mutation_chars.append(char)

    mutated_individual = Individual(''.join(individual_mutation_chars))
    return mutated_individual

def evaluate_population(population, target):
    avg_fit = 0
    for i in range(len(population)):
        fit = similar(population[i].string, target)
        population[i].fitness = fit
        avg_fit += fit
    avg_fit /= len(population)
    return population, avg_fit

# chapter: 6, page: 150
def next_generation(population, size=100, length=26, mutation_rate=0.01):
    new_population = []
    while len(new_population) < size:
        parents = random.choices(population, k=2, weights=[individual.fitness for individual in population])
        offspring = recombine(parents[0], parents[1])
        child_1 = mutate(offspring[0], mutation_rate)
        child_2 = mutate(offspring[1], mutation_rate)
        offspring = [child_1, child_2]
        new_population.extend(offspring)
    return new_population

num_generations = 100
# population_size = 100 - plot jagged, page: 151
population_size = 3000
str_len = len(target)
# mutation_rate = 0.1 - plot jagged, page: 151

# chapter: 6, page: 151 
# ako je graf nazubljen razlog moze biti ili premali stepen mutacije sto dovodi do velike varijanse ili premala velicina populacije
# sa druge strane ako plot jako brzo konvergira razlog moze biti premali stepen mutacije, jer jednostavno ne dajemo algoritmu mogucnost da istrazi nove osobine
mutation_rate = 0.001

population_fit = []
population = spawn_population(str_len, population_size)
for gen in range(num_generations):
    population, avg_fit = evaluate_population(population, target)
    population_fit.append(avg_fit)
    new_population = next_generation(population, population_size, str_len, mutation_rate)
    population = new_population

population.sort(key=(lambda individual: individual.fitness), reverse=True)
print(population[0].string)

fig = plt.figure(figsize=(10, 5))
ax = fig.add_subplot(1, 1, 1)
ax.title.set_text('Evolutionary algorithm(Genetic algorithm) - strings example')
ax.set_xlabel("Generations", fontsize=22)
ax.set_ylabel("Fitness", fontsize=22)
ax.plot(population_fit, color='green')

plt.tight_layout()
plt.show()



import numpy as np
import torch
from matplotlib import pyplot as plt

# chapter: 6, page: 152
def model(state, unpacked_params):
    weights_layer_1, bias_layer_1, weights_layer_2, bias_layer_2, weights_layer_3, bias_layer_3 = unpacked_params
    y = torch.nn.functional.linear(state, weights_layer_1, bias_layer_1)
    y = torch.relu(y)
    y = torch.nn.functional.linear(y, weights_layer_2, bias_layer_2)
    y = torch.relu(y)
    y = torch.nn.functional.linear(y, weights_layer_3, bias_layer_3)
    y = torch.log_softmax(y, dim=0)
    return y

def unpack_params(params, layers=[(4, 25), (25, 10), (10, 2)]):
    # size of params [4*25+25+25*10+10+10*2+2] = [407]
    # 4*25 - tezine na prvom lejeru, 25 biasi na prvom lejeru
    # 25*10 - tezine na drugom lejeru, 10 biasi na drugom lejeru
    # 10*2 - tezine na trecem lejeru, 2 biasi na trecem lejeru
    unpacked_params = []
    e = 0
    for _, l in enumerate(layers):
        # s, e = e, e + np.prod(l)
        s, e = e, e + l[0]*l[1]
        weights = params[s:e].view((l[1], l[0]))
        s, e = e, e + l[1]
        bias = params[s:e].view(-1)
        unpacked_params.extend([weights, bias])
    return unpacked_params

# chapter: 6, page: 153
def spawn_population(N=50, size=407):
    population = []
    for _ in range(N):
        # delimo sa 2.0 jer torch.randn() je fn koja vraca array sa elementima cije su vrednosti u rasponu koji 
        # ima normalna raspodela sa mean 0 i varijansom 1
        # https://homepage.divms.uiowa.edu/~mbognar/applets/normal.html
        params = torch.randn(size) / 2.0        
        fit = 0.0
        individual = {'params': params, 'fitness': fit}
        population.append(individual)
    return population

def recombine(parent_1, parent_2):
    params_p1 = parent_1['params']
    params_p2 = parent_2['params']

    l = params_p1.shape[0]

    split_point = np.random.randint(l)

    child_1_params = torch.zeros(l)
    child_2_params = torch.zeros(l)

    child_1_params[0:split_point] = params_p1[0:split_point]
    child_1_params[split_point:] = params_p2[split_point:]
    child_2_params[0:split_point] = params_p2[0:split_point]
    child_2_params[split_point:] = params_p1[split_point:]

    child_1 = {'params': child_1_params, 'fitness': 0.0}
    child_2 = {'params': child_2_params, 'fitness': 0.0}

    return child_1, child_2

# chapter: 6, page: 154
def mutate(individual, mutation_rate=0.01):
    params = individual['params']
    num_to_mutate = int(params.shape[0]*mutation_rate)
    indexes = np.random.randint(low=0, high=params.shape[0], size=(num_to_mutate,))
    params[indexes] = torch.randn(num_to_mutate) / 10.0
    return individual

import gym
env = gym.make('CartPole-v0')

def test_model(agent):
    done = False
    state = torch.from_numpy(env.reset()).float()
    score = 0
    while not done:
        params = unpack_params(agent['params'])
        log_probs = model(state, params)
        action = torch.distributions.Categorical(logits=log_probs.view(-1)).sample()
        new_state, reward, done, info = env.step(action.item())
        state = torch.from_numpy(new_state).float()
        score += 1
    return score

# chapter: 6, page: 155
def evaluate_population(population):
    tot_fit = 0
    for agent in population:
        score = test_model(agent)
        agent['fitness'] = score
        tot_fit += score
    avg_fit = tot_fit/len(population)
    return population, avg_fit

# chapter: 6, page: 156
def next_generation(population, mutation_rate=0.001, tournament_size=0.2):
    new_population = []
    lp = len(population)
    while len(new_population) < lp:
        random_ids = np.random.randint(low=0, high=lp, size=(int(tournament_size*lp),))
        batch = np.array([[i, agent['fitness']] for (i, agent) in enumerate(population) if i in random_ids])
        
        indexes = batch[:, 1].argsort()
        scores = batch[indexes]
        parent_1_id, parent_2_id = int(scores[-1][0]), int(scores[-2][0])
        parent_1, parent_2 = population[parent_1_id], population[parent_2_id]
        offspring = recombine(parent_1, parent_2)
        child_1 = mutate(offspring[0], mutation_rate)
        child_2 = mutate(offspring[1], mutation_rate)
        offspring = [child_1, child_2]
        new_population.extend(offspring)
    return new_population

num_generations = 25
population_size = 500
mutation_rate = 0.01
pop_fit = []
pop = spawn_population(population_size, 407)
for i in range(num_generations):
    pop, avg_fit = evaluate_population(pop)
    pop_fit.append(avg_fit)
    pop = next_generation(pop, mutation_rate, 0.2)

fig = plt.figure(figsize=(10, 5))
ax = fig.add_subplot(1, 1, 1)
ax.title.set_text('Agents learns to play CartPole - Evolutionary algorithm - Genetic algorithm')
ax.set_xlabel("Generations", fontsize=22)
ax.set_ylabel("Fitness", fontsize=22)
ax.plot(pop_fit, color='green')

plt.tight_layout()
plt.show()
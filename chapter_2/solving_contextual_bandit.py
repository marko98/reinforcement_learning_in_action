# * Beleske *

# - page: 39
# Stanje u igri je skup informacija dostupnih unutar okruzenja, koje mogu biti uzete u obzir prilikom donosenja odluke.

# RL algoritmi modeluju svet preko seta stanja(prostor stanja - set feature-a o okruzenju), seta akcija(prostor akcija - akcije koje mozemo preduzeti u trenutom stanju),
# nagrada(dobijaju se na osnovu stanja u kojem se nadjemo nakon preduzimanja jedne od mogucih akcija u prethodnom stanju)

# Do sada smo imali problem(slot masine) u kojima smo imali jedno stanje i 10 mogucih akcija, ukoliko bismo zeleli da resimo problem koji ima 100 stanja i u svakom stanju
# 10 mogucih akcija imali bismo 100*10 mogucih state-action parova. Lookup table-a nije bas korisna za resavanje problema sa ovolikim prostorom stanja i akcija, umesto nje
# koristicemo duboke neuronske mreze. DNNs kada se treniraju ispravno u mogucnosti su nauce apstrakcije, generalnije stvari(pravila), a ne da se koncentrisu na sitne detalje.
# Mogu da nauce kompozitne paterne i regulative i da na taj nacin od velike kolicine podataka pamte samo ono sto je zaista bitno. NNs mogu da nauce kompleksne relacije izmedju
# state-action parova i nagrada bez da skladiste svo znanje(informacije koje dobijaju) u memoriju.

import torch
import numpy as np
import random
import matplotlib.pyplot as plt

# contextual bandit environment, chapter: 2, page: 42
class ContextualBandit:
    def __init__(self, arms=10):
        self.arms = arms
        self.__init_distribution(self.arms)
        self.__update_state()
    
    def __init_distribution(self, arms):
        # number of states = number of arms, to keep things simple.
        # each row represents a state and each column an arm.
        self.bandit_matrix = np.random.rand(arms, arms)

    def __update_state(self):
        self.state = np.random.randint(0, self.arms)

    def get_state(self):
        return self.state

    def __reward(self, prob):
        reward = 0
        for _ in range(self.arms):
            if random.random() < prob:
                reward += 1
        return reward

    def choose_arm(self, arm):
        # choosing an arm returns a reward and updates the state.
        reward = self.__reward(self.bandit_matrix[self.get_state()][arm]) # u neku ruku moze se smatrati state-action-value fn
        self.__update_state()
        return reward

## environment example
# env = ContextualBandit(arms=10)
# state = env.get_state()
# reward = env.choose_arm(1)
# print(state)
# print(reward)

arms = 10
epohe = 5000
N, D_in, H, D_out = 1, arms, 100, arms # N, D_in, H, D_out - batch size, input dimension, hidden dimension, output dimension

# agent (nn), chapter: 2, page: 45
model = torch.nn.Sequential(
    torch.nn.Linear(D_in, H), 
    torch.nn.ReLU(), 
    torch.nn.Linear(H, D_out), 
    torch.nn.ReLU()
)
loss_fn = torch.nn.MSELoss()

env = ContextualBandit(arms)

def one_hot(N, pos, val=1):
    # np.zeros prima kao prvi param shape novog niza
    one_hot_vec = np.zeros(N)
    one_hot_vec[pos] = val
    return one_hot_vec

def softmax(vrednosti_akcija, tau=1.12):
    return np.exp(vrednosti_akcija/tau)/np.sum(np.exp(vrednosti_akcija/tau))

# chapter: 2, page: 46
def train(env, epochs=5000, learning_rate=1e-2):
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
    rewards = []
    for _ in range(epochs):
        cur_state = torch.Tensor(one_hot(arms, env.get_state()))
        y_pred = model(cur_state)
        av_softmax = softmax(y_pred.data.numpy(), tau=2.0)
        av_softmax /= av_softmax.sum() # normalizes distribution to make sure it sums to 1
        choice = np.random.choice(arms, p=av_softmax) # chooses new action probabilistically
        reward = env.choose_arm(choice)
        one_hot_reward = y_pred.data.numpy().copy()
        one_hot_reward[choice] = reward
        y = torch.Tensor(one_hot_reward)
        rewards.append(reward)
        loss = loss_fn(y_pred, y)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
    return rewards

rewards = train(env, epohe)

fig = plt.figure(figsize=(10, 5))
ax = fig.add_subplot(1, 1, 1)
ax.set_xlabel("Igra")
ax.set_ylabel("Prosecne nagrade")
ax.grid()

def average_sum(array, step):
    result = []
    i = 0
    k = step
    
    while k <= len(array):
        result.append(sum([i for i in array[i:k]])/step)
        i = k
        k += step

    return result

y = average_sum(rewards, 50)
x = np.arange(0, epohe, 50)
ax.scatter(x, y, cmap="summer", edgecolor="black", linewidth=1, alpha=0.75)

plt.tight_layout()
plt.show()

## chapter: 2, page: 41
# y_known = torch.Tensor([4, 8])
# x = torch.Tensor([2, 4]) # input data

# m = torch.randn(2, requires_grad=True)
# b = torch.randn(1, requires_grad=True)
# y = m*x+b # linear model
# loss = (torch.sum(y_known-y))**2 # loss function
# loss.backward() # calculate gradients
# print(m.grad)
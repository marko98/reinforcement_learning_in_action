# * Beleske *

# Deep Reinforcement Learning - Duboko ojacano ucenje
# Duboko ojacano ucenje: duboko ucenje + ojacano ucenje(agent utice na okolinu i okolina utice na agenta)

# Neuronske mreze mogu da imaju vise slojeva(layer) otuda naziv duboke neuronske mreze(Deep NNS).

# Ojacano ucenje(RL): ucenje kod kojeg mi neke akcije podsticemo, a neke omalovazavamo spram nagrada koje agent primi od okruzenja.

# Agent je drugi naziv za algoritam, f-ju koja 'uci' da obavlja neki zadatak.

# Okruzenje je bilo koji dinamican proces koji menja stanja, izvrsava moguce akcije u trenutnom stanju i nagradjuje agenta za 
# tranzicije(prelazak iz stanja u stanje) koje je ucinio. Nagrade se dobijaju za stanje u koje predjemo nakon izvrsenja odabrane akcije 
# u prethodnom stanju.

# Cilj ojacanog ucenja jeste da agent nauci kako da maksimalno poveca konacnu nagradu(suma svih prethodnih).

# - page: 9
# Dinamicno programiranje(DP) - Richard Bellman, 1957 - pristup u resavanju problema, ideja: resavati slozen problem razlaganjem njega
# na manje, jednostavnije podprobleme sve dok ne dodjemo do podproblema koji znamo kako da resimo. Bolji naziv za DP bi bio razlaganje ciljnog problema.

# DP ne moze da se primeni u RL, makar ne na nacin na koji ga je Richard Bellman izneo. DP se moze smatrati kao ekstrem medju tehnikama za resavanje
# problema. Suprotno od DP-a, imamo tehniku resavanja problema, gde ucimo na osnovu pokusaja i gresaka(Trial and error strategy).

# - page: 10
# Moguce je da se nadjemo u nekom okruzenju za koje imamo dovoljnu kolicinu znanja da bismo delovali unutar njega('imamo jasan model okruznja u nasoj glavi'), 
# ali isto tako moguce je da se nadjemo u okruzenju za koje nemamo dovoljnu kolicinu znanja da bismo znali da delujemo unutar njega. Dakle moracemo da primenjujemo
# razlicite strategije u zavisnosti od situacije u kojoj se nalazimo.

# Strategija pokusaja i gresaka(Trial and error strategy) spada u Monte Carlo metode. Monte Carlo metode su metode uz pomoc kojih na slucajan nacin(random) prikupljamo
# informacije o okruzenju.

# Ono sto je uglavnom slucaj u realnom svetu je da se nadjemo u okruznju u kojem imamo makar neku kolicinu znanja o okruzenju u kojem se nalazimo, pa mozemo da primenjujemo
# i jednu(DP, goal decomposition) i drugu strategiju(Trial and error strategy(Monte Carlo method)), odnosno da ucimo na osnovu gresaka i pokusaja i da resavamo neke pod 
# probleme na osnovu znanja koje imamo. Dakle imacemo i istrazivanje, ali i eksploataciju.

# Moze se raspravljati o tome da je vazniji Bellman-ov doprinos bio stvaranje termina i koncepata kroz koje se svaki problem u RL-u moze predstaviti. To nas dovodi to
# standardizovanog jezika za medjusobnu komunikaciju i uslovljava da definisemo nase probleme na nacin podlozan DP-u, odnosno da mozemo razlagati problem na manje podprobleme
# i iterirati kroz njih kako bismo ucili o njima i postizali vece uspehe u njihovom resavanju, a samim i u resavanju glavnog problema(cilj).

# - page: 11
# Da ponovimo: okruznje je sve sto stvara neke informacije od znacaja za nas(np. pri resavanju nekog problema).

# Stanje predstavlja sve informacije koje nam daje okruznje u odredjenom trenutku, koje mi potom prosledjujemo nasem algoritmu(agentu u nasem slucaju) 
# da bi doneo neku odluku na osnovu koje mi dalje delujemo u okruzenju.

# Glavna razlika izmedju RL-a i nadgledanog ucenja(supervised learning) je da kod zadatak koji iziskuju neku kontrolu algoritam treba da donese odluku i preduzme akciju.

# Nagrada je signal koji nam govori koliko dobro nas algoritam funkcionise, odnosno postize glavni cilj. Nagrada moze biti i pozitivna i negativna i nas agent je dobija
# od okruzenja nakon svake akcije koju preduzme.

# - page: 13
# Ono sto treba imati na umu je da se RL vise odnosi na tip problema i nacina njegovog resavanja, nego na sam algoritam ucenja.

# lookup table - tabela u koju smestamo nase znanje(korisna za probleme koji nemaju puno stanja i akcija)

import random
import numpy as np
import matplotlib.pyplot as plt

# chapter: 2, page: 33

# functions
def dobavi_najbolju_akciju(istorija):
    return np.argmax(istorija[:, 1], axis=0)

def dobavi_nagradu(verovatnoca, n=10):
    nagrada = 0
    for _ in range(n):
        if random.random() < verovatnoca:
            nagrada += 1
    return nagrada

def azuriraj_istoriju(istorija, akcija, nagrada):
    prosecna_nagrada = (istorija[akcija, 0] * istorija[akcija, 1] + nagrada) / (istorija[akcija, 0]+1)
    istorija[akcija, 0] += 1
    istorija[akcija, 1] = prosecna_nagrada
    return istorija


fig = plt.figure(figsize=(15, 5))
ax = fig.add_subplot(1, 2, 1)
ax.set_xlabel("Igra")
ax.set_ylabel("Prosecne nagrade")
ax.grid()

broj_slot_masina = 10

istorija = np.zeros((broj_slot_masina, 2)) # nasa lookup tabela
verovatnoce_nagrada_za_svaku_slot_masinu = np.random.rand(broj_slot_masina)

# da bismo imali i istrazivanje, ali i eksploataciju
# epsilon-greedy strategy(policy)
epsilon = 0.2
prosecne_nagrade = [0]

for i in range(1, 501):
    if random.random() > epsilon:
        akcija = dobavi_najbolju_akciju(istorija)
    else:
        akcija = np.random.randint(0, broj_slot_masina)
    
    nagrada = dobavi_nagradu(verovatnoce_nagrada_za_svaku_slot_masinu[akcija])
    istorija = azuriraj_istoriju(istorija, akcija, nagrada)
    prosecna_nagrada = (i * prosecne_nagrade[-1] + nagrada) / (i+1)
    prosecne_nagrade.append(prosecna_nagrada)

# np.arange() ako se prosledi jedan arg, smatra se da je donja granica 0
ax.scatter(np.arange(len(prosecne_nagrade)), prosecne_nagrade, cmap="summer", edgecolor="black", linewidth=1, alpha=0.75)

# using softmax fn, chapter: 2, page: 37

# functions
def softmax(vrednosti_akcija, tau=1.12):
    return np.exp(vrednosti_akcija/tau)/np.sum(np.exp(vrednosti_akcija/tau))

ax = fig.add_subplot(1, 2, 2)
ax.set_xlabel("Igra")
ax.set_ylabel("Prosecne nagrade")
ax.grid()

broj_slot_masina = 10

istorija = np.zeros((broj_slot_masina, 2))
verovatnoce_nagrada_za_svaku_slot_masinu = np.random.rand(broj_slot_masina)

prosecne_nagrade = [0]

for i in range(1, 501):
    # softmax selection policy
    probability_vector = softmax(istorija[:, 1])
    akcija = np.random.choice(np.arange(broj_slot_masina), p=probability_vector)

    nagrada = dobavi_nagradu(verovatnoce_nagrada_za_svaku_slot_masinu[akcija])
    istorija = azuriraj_istoriju(istorija, akcija, nagrada)
    prosecna_nagrada = (i * prosecne_nagrade[-1] + nagrada) / (i+1)
    prosecne_nagrade.append(prosecna_nagrada)

# np.arange() ako se prosledi jedan arg, smatra se da je donja granica 0
ax.scatter(np.arange(len(prosecne_nagrade)), prosecne_nagrade, cmap="summer", edgecolor="black", linewidth=1, alpha=0.75)


plt.tight_layout()
plt.show()

# # example of softmax fn
# # parametar tau skalira raspodelu verovatnoce akcija na osnovu njihovih vrednosti
# # sto je tau veci to ce razlika izmedju verovatnoca biti manja(odnosno nece dolaziti toliko do izrazaja) i obrnuto
# def softmax(akcija_vrednosti, tau=1.12):
#     return np.exp(akcija_vrednosti/tau)/np.sum(np.exp(akcija_vrednosti/tau))

# akcije = np.arange(10)
# # akcija_vrednosti = np.zeros(10)
# akcija_vrednosti = np.array([0,0,0,0,1,0,0,0,4,0])
# probability_vector = softmax(akcija_vrednosti)
# print(probability_vector)
# izabrane_akcije = []
# for i in range(50):
#     izabrane_akcije.append(np.random.choice(akcije, p=probability_vector))
# print(max(set(izabrane_akcije), key = izabrane_akcije.count))
# print(sorted(izabrane_akcije))
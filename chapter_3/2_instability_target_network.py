from collections import deque
import numpy as np
import matplotlib.pyplot as plt
import torch
import random
import copy

from Gridworld import Gridworld
from test_model import test_model

# chapter: 3, page: 82

IN_DIM, OUT_DIM = 64, 4
L1_OUT_DIM = 150
L2_IN_DIM = L1_OUT_DIM
L2_OUT_DIM = 100
L3_IN_DIM = L2_OUT_DIM

model = torch.nn.Sequential(
    torch.nn.Linear(IN_DIM, L1_OUT_DIM), 
    torch.nn.ReLU(),
    torch.nn.Linear(L2_IN_DIM, L2_OUT_DIM),
    torch.nn.ReLU(),
    torch.nn.Linear(L3_IN_DIM, OUT_DIM)
)
# print(model)

target_model = copy.deepcopy(model) # copy network
target_model.load_state_dict(model.state_dict()) # copy network's params

loss_fn = torch.nn.MSELoss()
learning_rate = 1e-3
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

gamma = 0.9
epsilon = 1.0

action_set = {
    0: 'u',
    1: 'd',
    2: 'l',
    3: 'r'
}

epochs = 5000
losses = []
mem_size = 1000
batch_size = 200
replay = deque(maxlen=mem_size)
max_moves = 50
h = 0
sync_freq = 50
j = 0

moj_j = 0
for i in range(epochs):
    game = Gridworld(size=4, mode='random')
    state = game.board.render_np().reshape(1, 64) + np.random.rand(1,64)/100.0
    state = torch.from_numpy(state).float()
    status = 1
    move = 0

    epoch_losses = []
    while(status == 1) :
        move += 1
        j += 1
        q_values = model(state)
        q_values_np = q_values.data.numpy()
        if(random.random() < epsilon):
            action_key = np.random.randint(0, 4)
        else:
            action_key = np.argmax(q_values_np)
        
        action = action_set[action_key]
        game.makeMove(action)
        reward = game.reward()
        new_state = game.board.render_np().reshape(1, 64) + np.random.rand(1, 64)/100.0
        new_state = torch.from_numpy(new_state).float()
        done = True if reward > 0 else False
        experience = (state, action_key, reward, new_state, done)
        replay.append(experience)
        state = new_state

        if len(replay) > batch_size:
            minibatch = random.sample(replay, batch_size)
            state_batch = torch.cat([s for (s, a, r, ns, d) in minibatch])
            action_batch = torch.Tensor([a for (s, a, r, ns, d) in minibatch])
            reward_batch = torch.Tensor([r for (s, a, r, ns, d) in minibatch])
            new_state_batch = torch.cat([ns for (s, a, r, ns, d) in minibatch])
            done_batch = torch.Tensor([d for (s, a, r, ns, d) in minibatch])

            Q1 = model(state_batch)
            with torch.no_grad():
                Q2 = target_model(new_state_batch) # koristimo 'target' mrezu za dobavljanje Q vrednosti za sledece stanje u kojem ce se model(glavna mreza) naci
            
            Y = reward_batch + gamma * ((1 - done_batch) * torch.max(Q2, dim=1)[0])
            a = action_batch.long().unsqueeze(1)
            X = torch.gather(Q1, 1, a).squeeze(1)
            loss = loss_fn(X, Y.detach())
            optimizer.zero_grad()
            loss.backward()
            epoch_losses.append(loss.item())
            optimizer.step()

            if j % sync_freq == 0:
                target_model.load_state_dict(model.state_dict())

        if reward != -1 or move > max_moves:
            status = 0
            move = 0

    if(sum(epoch_losses) > 0):
        # losses.append({'epoch': moj_j, 'loss': sum(epoch_losses)/len(epoch_losses)})
        losses.append({'epoch': moj_j, 'loss': sum(epoch_losses)})
        epoch_losses = []
        moj_j += 1

    if epsilon > 0.1:
        epsilon -= (1/epochs)

fig = plt.figure(figsize=(10, 5))
ax = fig.add_subplot(1, 1, 1)
ax.title.set_text('Agent learns to play Gridworld - catastrophic forgetting and experience replay')
ax.set_xlabel("Epochs")
ax.set_ylabel("Loss")
# ax.bar(np.arange(0, epochs), list(map(lambda x: x['loss'], losses)), color="b")
ax.bar(list(map(lambda x: x['epoch'], losses)), list(map(lambda x: x['loss'], losses)), color="#89083A")

plt.tight_layout()
plt.show()

# testing
max_games = 1000
wins = 0
for i in range(max_games):
    win = test_model(target_model, mode='random', display=False)
    if win:
        wins += 1
win_perc = float(wins) / float(max_games)
print("Games played: {0}, # of wins: {1}".format(max_games,wins))
print("Win percentage: {}%".format(100.0*win_perc))

# za vise pogledaj chapter: 3, page: 86
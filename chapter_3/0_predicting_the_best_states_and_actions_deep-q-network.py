# * Beleske *

# - page: 47
# Markovljeva osobina(Markov property - MP)

# Igra(neki kontrolni zadatak) koja ispoljava Markvljevu osobinu moze se posmatrati kao Markovljev proces odlucivanja(MDP - Markov decision process).
# Ako je neki proces Markovljev proces odlucivanja, onda znamo da trenutno stanje sadrzi dovoljnu kolicinu informacija da bismo odabrali najbolju akciju, odnosno
# ne treba nam istorija da bismo donosili odluke u okruzenju.

# MDP model pojednostavljuje RL problem drasticno - tako sto kaze da ne moramo da uzimamo u obzir prethodna stanja i akcije, odnosno ne treba nam memorija, samo treba da
# analiziramo trenutno stanje. Ideja: svaki problem treba pokusati svesti na MDP.

# - page: 48
# neki od problema:

# * bitno! - o svemu ovome se da diskutovati
# voznja automobila - ima MP, nije nam potrebno da znamo sta se desilo pre 10min da bismo optimalno vozili automobil
# da li ulagati u neke akcije na berzi ili ne - nema MP buduci da bismo zeleli da imamo prosla 'kretanja' akcija na berzi uz trenutno 'kretanje' da bismo doneli odluku
# biranje medicinskog lecenja za pacijenta - ima MP buduci da ne moramo da znamo istoriju pacijenta da bismo zakljucili sta ga boli
# dijagnoza bolesti pacijenta - nema MP buduci da je veoma vazno da znamo prethodna stanja bolesti da bismo odredili dijagnozu
# predvidjanje pobednickog tima u fudbalu - nema MP buduci da nam je bitno i da znamo prethodne uspehe tima da bismo dobro predvidjali(slicno kao kod akcija na berzi)
# odabir najkrace rute do neke destinacije - ima MP buduci da nam je bitno samo da znamo rastojanje koje ne zavisi od toga sta se juce desilo
# nisanjenje mete oruzijem - ima MP buduci da nam je potrebano da znamo gde se meta nalazi mozda brzinu vetra, ali ne i brzinu vetra prethodnog dana

# Imajte na umu da mnogi problemi ne mogu da zadovolje MP, ali to ne znaci da ne mozemo mi da ih svedemo na MDP(odnosno da zadovoljavaju MP) - to mozemo postici
# tako sto cemo potrebne dodatne informacije dodati u stanje.

# Razmotrimo DeepMind's deep Q-network algoritam koji je naucio da igra Atari igre na osnovu piksela. Da li Atari igre imaju MP? Nemaju. Ako pogledamo Pacman-a mi mozemo videti
# gde se nalaze protivnici na osnovu piksela sa trenutnog frejma, ali ne mozemo zakljuciti da li se krecu ka nama ili od nas, sto nam je bitna informacija za preduzimanje akcije.
# Medjutim ako za stanje uzememo poslednja 4 frejma, onda imamo pristup pravcima i smerovima kao i brzinama kretanja protivnika. Na ovaj nacin igranje igre Pacman, 
# koja nema MP je dobija i moze se smatrati MDP-om.

# - page: 49
# U realnom svetu, preduzimanje akcija u nekom stanju nam ne garantuje prelazak u zeljeno stanje. 
# Kada preduzmemo neku akciju u trenutnom stanju ono sto nam je zagarantovano je da cemo preci u neko stanje(to moze biti opet trenutno stanje), ali nam nije zagarantovano koje. 
# Razlog za to jesu verovatnoce tranzicija(transition probabilities) i suma verovatnoca svih tranzicija mora biti 1(pogledati MDP_dijagram.png). 
# Dakle ono sto mozemo ocekivati jeste da prilkom odabira akcije u trenutnom stanju sa odredjenom verovatnocom cemo se naci u zeljenom stanju.

# Kada bismo znali verovatnoce tranzicija onda bismo imali model okruzenja, tj. agent bi ga imao. Ukoliko nas agent nema model okruzenja u kojem se nalazi, moguce je da bismo zeleli
# da ga nauci.

# - page: 50
# Da ponovimo:
# - nas agent je RL algoritam koji deluje u nekom okruznju
# - okruznje je cesto neka igra, ali opstije gledano to je proces koji proizvodi stanja, akcije i nagrade
# - agent ima pristup trenutnom stanju okruzenja, sto su podaci koje okruzenje trenutno poseduje
# - na osnovu stanja, agent preduzima odredjenu akciju koja deterministicki ili probabilisticki menja okruznje, odnosno okruzenje ce se naci u novom stanju(St+1)
# - verovatnoca koja je povezana sa mapiranjem stanja na novo stanje preduzimanjem odredjene akcije naziva se verovatnoca tranzicije
# - agent dobija nagradu za preduzimanje odredjene akcije(At) u stanju(St) koja nas vodi do novog stanja(St+1)
# - ono sto daje nagradu jeste tranzicija iz St -> St+1, dakle nije akcija buduci da ona moze sa odredjenom verovatnocom da nas odvede u lose stanje
# - nagrada je vezana za stanje u koje predjemo iz prethodnog stanja
# - cilj nam je da agent maksimalno poveca ukupnu nagradu

# Policy functions(strategije)
# Polisa(𝜋) je strategija agenta u okruzenju.
# U prethodnim primerima implementirali smo dve strategije - epsilon-greedy strategija i softmax selection strategija.

# Policy fn je funkcija koja moze da ili mapira stanje na skup(ili listu) raspodele verovatnoca mogucih akcija u datom stanju ili da mapira stanje na akciju koju da preduzmemo.
# Cesto se za policy fn uzima epsilon-greedy policy.
# Tada sa verovatnocom ε preduzimamo random akciju u trenutnom stanju(dobijamo istrazivanje), a sa verovatnocom ε-1 preduzimamo najbolju akciju spram znanja koje 
# imamo(dobijamo eksploataciju), na pocetku vazi ε=1, a kako epohe budu prolazile tako ga smanjujemo kao npr. 0.1.

# - page: 51
# Optimal policy - polisa(strategija) koja najbolje povecava nagrade.

# Postoje dva nacina na koje mozemo da testiramo naseg agenta:
# 1) direktno - ucimo agenta koje akcije su najbolje u trenutnom stanju
# 2) indirektno - ucimo agenta koja stanja su najbolja(donose najvise nagrada), a potom da preduzme akcije koje ga vode do tih najboljih stanja - ovaj nacin je doveo do 
# ideje o value f-ji

# Value fn je funkcija koja mapira ili stanje na vektor ocekivanih nagrada(Q values) ili state-action par na ocekivanu nagradu(Q value), ovaj drugi nacin se prvi koristio.
# Value fn moze da se predstavi kao:
# state-action-value fn = action value fn = Q𝜋(s,a) = Q(s,a) => daje nam Q value(expected reward)
# ili
# state-value fn = value fn = V𝜋(s) = QA(s) => daje nam vektor ocekivanih nagrada(vector of Q values) * QA(s) - A predstavlja skup svih mogucih akcija u stanju s

# - page: 52
# Za koji god tip value f-je se odlucili ona nam kao izlaz vraca Q value/s i zbog toga se cesto naziva Q fn.

# - page: 56
# Q-learning algoritam je veoma star i sluzi kao metoda da se nauce optimalne akcije spram nagrada koje dobijamo za njihovo preduzimanje. 
# - page: 57
# Value fns i action value fns su uposteno govoreci koncepti u RL. Q-learning algoritam koristi te koncepte.

# Q-learning update rule:
# Q(St,At) = Q(St,At) + α[Rt+1 + γ max Q(St+1,a) - Q(St,At)]

import numpy as np
import matplotlib.pyplot as plt
import torch
import random

from Gridworld import Gridworld

# # basic operations from Gridworld module
# # chapter 3, page 63
# game = Gridworld(size=4, mode='static')
# # modes: 'static' - objekti su uvek na istim pozicijama inicijalizovani, 'player' - igrac je na random poziciji inicijalizovan, 
# # 'random' - svi objekti su na random pozicijama inicijalizovani
# print(game.display())
# game.makeMove('d')
# game.makeMove('d')
# game.makeMove('l')
# print(game.display())
# print(game.reward())
# print(game.board.render_np())
# print(game.board.render_np().shape)
# print(game.board.render_np().reshape(1, 64))

# chapter 3, page 66
IN_DIM, OUT_DIM = 64, 4
L1_OUT_DIM = 150
L2_IN_DIM = L1_OUT_DIM
L2_OUT_DIM = 100
L3_IN_DIM = L2_OUT_DIM

model = torch.nn.Sequential(
    torch.nn.Linear(IN_DIM, L1_OUT_DIM), 
    torch.nn.ReLU(),
    torch.nn.Linear(L2_IN_DIM, L2_OUT_DIM),
    torch.nn.ReLU(),
    torch.nn.Linear(L3_IN_DIM, OUT_DIM)
)
# print(model)
loss_fn = torch.nn.MSELoss()
learning_rate = 1e-3
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

gamma = 0.9
epsilon = 1.0

action_set = {
    0: 'u',
    1: 'd',
    2: 'l',
    3: 'r'
}

epochs = 1000
losses = []
for i in range(epochs):
    # jedna epoha nam dodje jedna epizoda odnosno jedna igra dok se ne zavrsi(konkretno odigracemo 1000 puta igru)
    game = Gridworld(size=4, mode='static')
    # Dodajemo sum u trenutno stanje, jer zelimo da sprecimo 'mrtve neurone' koji mogu da nastanu zbog ReLU f-je koju koristimo za aktivaciju cvora. Buduci da su elementi u
    # nasem stanju vecinom 0, sem jedinice koja govori gde se nalazi objekat na tabli, ti elementi ce izgubiti pravu vrednost zbog nacina na koji ReLU f-ja radi. Tako da cemo
    # svakom elementu dodati malo suma da ne budu 0, to nam moze pomoci i da ne overfitujemo model.
    state = game.board.render_np().reshape(1, 64) + np.random.rand(1, 64) / 10.0
    state = torch.from_numpy(state).float()
    status = 1 # da bismo pratili progres igre(da li traje ili se zavrsila)
    losses.append({'loss': 0, 'action_count': 0, 'epoch': i})
    while status == 1:
        # dobavljamo ocekivane nagrade(expected rewards) za stanje u kojem se nalazimo, racunamo ocekivanu nagradu za svaku akciju koju mozemo da preduzmemo u trenutnom stanju
        q_values = model(state)
        q_values_np = q_values.data.numpy()
        if random.random() < epsilon:
            # istrazujemo(exploration), biramo random akciju bez obzira na ocekivane nagrade koje smo predvideli
            action_key = np.random.randint(0, 4)
        else:
            # eksploatisemo(exploitation), biramo akciju koja ce nam doneti najvecu ocekivanu nagradu
            action_key = np.argmax(q_values_np)
        
        action = action_set[action_key]
        game.makeMove(action)
        reward = game.reward()
        new_state = game.board.render_np().reshape(1, 64) + np.random.rand(1, 64) / 10.0
        new_state = torch.from_numpy(new_state).float()

        with torch.no_grad():
            new_q_values = model(new_state)

        max_new_q_value = torch.max(new_q_values)

        # racunamo target Q value(expected reward)
        if reward == -1:
            Y = reward + gamma * max_new_q_value
        else:
            # igra je gotova(ili smo pobedili ili izgubili)
            Y = reward

        Y = torch.Tensor([Y]).detach()
        X = q_values.squeeze()[action_key] # shape: 1 x 4 (matrix tensor), nakon squeeze() fn shape: 4 (vector tensor)
        loss = loss_fn(X, Y)
        optimizer.zero_grad()
        loss.backward()

        losses[-1]['loss'] += loss.item()
        losses[-1]['action_count'] += 1

        optimizer.step()
        state = new_state
        if reward != -1:
            status = 0
    
    if epsilon > 0.1:
        epsilon -= (1/epochs)

fig = plt.figure(figsize=(10, 5))
ax = fig.add_subplot(1, 1, 1)
ax.title.set_text('Agent learns to play Gridworld - DQN')
ax.set_xlabel("Epochs")
ax.set_ylabel("Loss")
# ax.bar(np.arange(0, epochs), list(map(lambda x: x['loss'], losses)), color="b")
ax.bar(list(map(lambda x: x['epoch'], losses)), list(map(lambda x: x['loss'], losses)), color="#89083A")

plt.tight_layout()
plt.show()

from test_model import test_model

test_model(model, 'static')

# print(torch.nn.MSELoss()(torch.Tensor([10]), torch.Tensor([3]))) # (10-3)**2